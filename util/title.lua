-- Use fist header as title

local title

function Meta(meta)
	meta.title = title
	return meta
end

function Header(el)
	if not title then
		title = el.content
		return {}
	end
end
